# README #

This repository provides an example implementation for messaging between MATLAB and a C++ process using Lightweight Communications and Marshalling ([LCM](http://lcm-proj.github.io)).

## C++ ##
Using the files found in cpp folder, follow LCM's C++ Tutorial found here:  

http://lcm-proj.github.io/tut_cpp.html

To compile call:

```
#!bash
$ gcc -o listener efc-lcm.cpp -llcm
```



## MATLAB ##
Now do the same with LCM's MATLAB Tutorial found here: 

http://lcm-proj.github.io/tut_matlab.html



You can add lcm.jar to your CLASSPATH like so:
```
#!bash
$ echo 'export CLASSPATH=".:/usr/local/share/java/lcm.jar:$CLASSPATH"' >> ~/.bash_profile
```


Once built, add my_types.jar to CLASSPATH to get lcm-spy to recognize lcm types: 
```
#!bash

$ export CLASSPATH=$CLASSPATH:/full/path/to/mytypes/my_types.jar
```


## Troubleshooting MATLAB/LCM issues ##
**1. Avoid crashing after initializing lcm object (lc = lcm.lcm.LCM.getSingleton();):**

 - Problem: ipv6 seems to be crashing Java, which crashes LCM and takes Matlab too

 - Solution: Default the JVM back to ipv4.

1. navigate to (or create if needed) java.opts inside $MATLABROOT/bin/$ARCH (in my case, MATLAB_R2016a.app/bin/maci64)

2. add following line to java.opts:
```bash
  -Djava.net.preferIPv4Stack=true
```

Find more details here:

- StackOverflow answer explained how to default the JVM back to ipv4: http://stackoverflow.com/questions/18747134/getting-cant-assign-requested-address-java-net-socketexception-using-ehcache

 - To get Matlab talking to JVM, we followed instructions here: http://www.mathworks.com/matlabcentral/answers/92813-how-do-i-increase-the-heap-space-for-the-java-vm-in-matlab-6-0-r12-and-later-versions



**2. Get compiled Java classes to work w/ MATLAB:**

- Problem: This could possibly be an issue with the version of the Java compiler (javac) installed on your system. MATLAB currently (R2016a) cannot run classes that have been compiled with the Java 1.7 (or free alternative compilers that are version 1.7 compliant). 

- Solution: You should retarget your compiling for version 1.6. This can be done by passing javac the --source=1.6 --target=1.6 flags.

EXAMPLE: 
```bash
$ javac -source 1.6 -target 1.6 -cp /usr/local/share/java/lcm.jar exlcm/*.java

# if you added lcm.jar to your CLASSPATH call:
$ javac -source 1.6 -target 1.6 exlcm/*.java
```