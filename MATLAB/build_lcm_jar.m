function [lc] = build_lcm_jar(types_lcm)

% build_lcm_jar.m
%--------------------------------------------------------------------------
% Initialize LCM .class and .jar files and add them to MATLAB's dynamic
% javaclasspath.
%
% Input:
% types_lcm = path to the LCM type specification file (e.g., example_t.lcm)
%
% Output:
% lc = intialized lcm object
%
% Last modified Aug 23, 2016
% Ben Raanan, MBARI
%--------------------------------------------------------------------------

if ischar(types_lcm)
    types_lcm = {types_lcm};
end

cwd = pwd();
for type_lcm = types_lcm(:)'
    
    type_lcm = type_lcm{:};
    
    [workd, name, ext] = fileparts(which(type_lcm));
    assert(length(name)>=2,'LCM type (.lcm) filename must be at least 2 charecters long.')
    addpath(genpath(workd))
    
    % Get lcm package name from types_lcm.lcm file
    %------------------------------------------------
    fid = fopen(fullfile(workd, [name ext]),'r');
    C = textscan(fid, '%s','Delimiter','');
    fclose(fid);
    C = C{:};
    
    id = ~cellfun(@isempty, strfind(C,'package '));
    lcm_name = regexp(C(id), '\package (.*?);','tokens');  % message: grab text between 'package ' and ;
    lcm_name = char(cellfun(@(c) c{1},lcm_name));
    
    
    lcm_types_jar = [workd filesep lcm_name '_types.jar'];
    if ~exist(lcm_types_jar, 'file')
        % cd into lcm dir
        %------------------------------------------------
        if ~strcmpi(workd,cwd)
            cd(workd)
        end
        
        % Add LCM to PATH
        %------------------------------------------------
        path1 = getenv('PATH');
        if isempty(strfind(path1, ':/usr/local/bin/'))
            path1 = [path1 ':/usr/local/bin/'];
            setenv('PATH', path1);
            fprintf('$PATH=')
            !echo $PATH
            fprintf('\n')
        end
        
        % Compile jar
        %------------------------------------------------
        system(['lcm-gen -j ' workd filesep type_lcm]);
        system(['javac -source 1.7 -target 1.7 -cp /usr/local/share/java/lcm.jar ' workd filesep lcm_name '/*.java']);
        system(['jar cf ' lcm_types_jar ' ' lcm_name '/*.class']);
    end
    
    
    % Add lcm jar files to MATLAB's java path
    %------------------------------------------------
    p = javaclasspath('-dynamic');
    lcm_jar = '/usr/local/share/java/lcm.jar';
    
    if ~any(strcmp(lcm_jar, p))
        javaaddpath(lcm_jar);
    end
    
    if ~any(strcmp(lcm_types_jar, p))
        javaaddpath(lcm_types_jar);
        % javarmpath(p{:}) % <- to remove jar from path
    end
    
    
    % Add lcm_types.jar file to CLASSPATH
    %------------------------------------------------
    cpath = getenv('CLASSPATH');
    if isempty(strfind(cpath, lcm_types_jar))
        cpath = [cpath [':' lcm_types_jar]];
        setenv('CLASSPATH', cpath);
        fprintf('$CLASSPATH=')
        !echo $CLASSPATH
        fprintf('\n')
    end
    
    addpath(genpath(workd))
    cd(cwd)
end

% Initialize lcm object
%------------------------------------------------
lc = lcm.lcm.LCM.getSingleton();

end
