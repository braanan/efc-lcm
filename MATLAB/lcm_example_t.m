% lcm_example_t.m

clear

% Add lcm folder to MATLAB path and load data
%------------------------------------------------
workd = fileparts(which('lcm_example_t.m'));
addpath(genpath(workd))
load([workd '/lcm_example_t_data.mat'])

% cd into lcm dir
%------------------------------------------------
if ~strcmpi(workd,pwd)
    cd(workd)
end

% Add lcm jar files to MATLAB's java path
%------------------------------------------------
p = javaclasspath('-dynamic');
if all(cellfun('isempty',p))
    javaaddpath('/usr/local/share/java/lcm.jar')
    javaaddpath my_types.jar
    % javarmpath(p{:}) % <- to remove jar from path
end

% Initialize lcm object
%------------------------------------------------
lc = lcm.lcm.LCM.getSingleton();


% Assemble message and publish to channel "EXAMPLE"
%--------------------------------------------------
k=3100;
while k<=len(matime)
    
    k = k+1;
    
    msg = exlcm.example_t();
    msg.matime = matime(k);
    msg.depth = depth(k);
    msg.depthRate = depthRate(k);
    msg.speedCmd = speedCmd(k);
    msg.pitchAngle = pitchAngle(k);
    msg.elevAngle = elevAngle(k);
    msg.verticalMode = verticalMode(k);
    msg.name = 'lcm_example_t';
    msg.enabled = 1;
    
    lc.publish('EXAMPLE', msg)
    pause(0.05)
end


%{
% Gen .mat data file
%------------------------------------------------
matime = x2.xst.time;
depth = x2.xst.z;
depthRate = x2.xst.dz;
speedCmd = x2.xst.speedCmd;
pitchAngle = x2.xst.theta;
elevAngle = x2.xst.elev;
verticalMode = x2.xst.verticalMode.verticalMode;

save('~/MATLAB/MBARI/lcm/lcm_example_t_data.mat',...
    'matime', 'depth', 'depthRate', 'speedCmd',...
    'pitchAngle', 'elevAngle', 'verticalMode');
%}

