//  efc-lcm.hpp
//

#ifndef EFC_LCM_HPP
#define EFC_LCM_HPP

#include <iostream>
#include <cmath>   // std::isnan
#include <numeric> // std::accumulate

#include <lcm/lcm-cpp.hpp>
#include "exlcm/example_t.hpp"


class lcmEFCListener
{
public:
    lcmEFCListener (int sampleSize);
    
    virtual ~lcmEFCListener ();
    
    // method for processing incoming messeges
    void processMessage (const lcm::ReceiveBuffer* rbuf,
                         const std::string &channel,
                         const exlcm::example_t* msg);
    
    void listen (const std::string channel);
    
    void runningAvg (const double& elevAngle);
    
    bool checkData (const exlcm::example_t* msg);
    
private:
    lcm::LCM lcm;
    int count;
    int sampleSize;
    double elevTmp;
    double elevOffset;
};

#endif // EFC_LCM_HPP