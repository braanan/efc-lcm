//
//  efc-lcm.cpp
//  lcm_example_t
//
// compile with:
//  $ gcc -o listener efc-lcm.cpp -llcm
//
// On a system with pkg-config, you can also use:
//  $ gcc -o listener efc-lcm.cpp `pkg-config --cflags --libs lcm`
//

#include "efc-lcm.hpp"

lcmEFCListener::lcmEFCListener(int N) :
lcm(lcm::LCM()),
count(0),
elevTmp(0)
{
    if(!lcm.good()) std::cout << "LCM would not initialize.";
    sampleSize = N;
}

lcmEFCListener::~lcmEFCListener() {}

void lcmEFCListener::processMessage(const lcm::ReceiveBuffer* rbuf,
                                    const std::string &channel,
                                    const exlcm::example_t* msg)
{
    if (checkData(msg)) {
        runningAvg(msg->elevAngle);
    }
}

void lcmEFCListener::listen(const std::string channel)
{
    lcm.subscribe(channel, &lcmEFCListener::processMessage, this);
    while(0 == lcm.handle());
}

bool lcmEFCListener::checkData(const exlcm::example_t* msg)
{
    bool datagood = false;
    bool dataCheck[7] = {false};
    
    if(!std::isnan(msg->speedCmd))  dataCheck[0] = true;
    if(!std::isnan(msg->depth))     dataCheck[1] = true;
    if(!std::isnan(msg->depthRate)) dataCheck[2] = true;
    if(!std::isnan(msg->elevAngle)) dataCheck[3] = true;
    if(!std::isnan(msg->pitchAngle)) dataCheck[4] = true;
    if(!std::isnan(msg->verticalMode)) dataCheck[5] = true;
    if(msg->depth > 1 && msg->speedCmd>0.8 && msg->verticalMode==5) dataCheck[6] = true;
    
    if(std::accumulate(dataCheck, dataCheck+7, 0.0) == 7)
        datagood = true;
    
    return datagood;
}

void lcmEFCListener::runningAvg(const double& elevAngle)
{
    count += 1; //pulse counter
    elevTmp += elevAngle; //sum-up elev angle for bin avg.
    
    if (count == sampleSize) {
        elevOffset = elevTmp / sampleSize;
        
        std::cout << "\nElevOffset = " << elevOffset << " (Sample size = " << sampleSize << ")\n\n";
        
        count = 0;
        elevTmp = 0;
        elevOffset = 0;
    }
}

int main(int argc, char* argv[]) {
    
    // this is a commandline input...
    int N = 100;
    std::string channel = "EXAMPLE";
    
    lcmEFCListener listener(N);
    listener.listen(channel);
    
    return 0;
}
